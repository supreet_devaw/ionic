angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller("ExampleController", function($scope,$cordovaBarcodeScanner) {
     $scope.scanBarcode = function() {
        $cordovaBarcodeScanner.scan().then(function(imageData) {
          if(!imageData.cancelled){
            if(imageData.format=="QR_CODE"){
              alert(imageData.text);
            } else {
              alert('not a qr code');

              $scope.scanBarcode();
            }
          }
        }, function(error) {
            alert(error);
            console.log("An error happened -> " + error);
        });
    };
})

.controller('ContactsController', function($scope, $cordovaContacts, $ionicPlatform) {
 
    $ionicPlatform.ready(function() {   
 
        $scope.contacts = {};  // We will use it to load contacts   
         
    $scope.contact = {     // We will use it to save a contact
 
        "displayName": "Gajotres",
        "name": {
            "givenName"  : "Dragan",
            "familyName" : "Gaic",
            "formatted"  : "Dragan Gaic"
        },
        "nickname": 'Gajotres',
        "phoneNumbers": [
            {
                "value": "+385959052082",
                "type": "mobile"
            },
            {
                "value": "+385914600731",
                "type": "phone"
            }               
        ],
        "emails": [
            {
                "value": "dragan.gaic@gmail.com",
                "type": "home"
            }
        ],
        "addresses": [
            {
                "type": "home",
                "formatted": "Some Address",
                "streetAddress": "Some Address",
                "locality":"Zagreb",
                "region":"Zagreb",
                "postalCode":"10000",
                "country":"Croatia"
            }
        ],
        "ims": null,
        "organizations": [
            {
                "type": "Company",
                "name": "Generali",
                "department": "IT",
                "title":"Senior Java Developer"
            }
        ],
        "birthday": Date("08/01/1980"),
        "note": "",
        "photos": [
            {
                "value": "https://pbs.twimg.com/profile_images/570169987914924032/pRisI2wr_400x400.jpeg"
            }
        ],
        "categories": null,
        "urls": null
    }           
                 
        $scope.addContact = function() {
            $cordovaContacts.save($scope.contact).then(function(result) {
                console.log('Contact Saved!');
            }, function(err) {
                console.log('An error has occured while saving contact data!');
            });
        };
 
        // This function can take some time  so be patient
        $scope.getAllContacts = function() {
            $cordovaContacts.find({filter : 'Robert', fields:  [ 'displayName']}).then(function(allContacts) { //omitting parameter to .find() causes all contacts to be returned
                $scope.contacts = allContacts;
                console.log(JSON.stringify(allContacts));
            });
        };
 
        $scope.removeContact = function() {
             
            $scope.removeContact = {};   // We will use it to save a contact
            $scope.removeContact.displayName = 'Gajotres'; // Contact Display Name          
             
            $cordovaContacts.remove($scope.removeContact).then(function(result) {
                console.log('Contact Deleted!');
                console.log(JSON.stringify(result));
            }, function(error) {
                console.log('An error has occured while deleting contact data!');
                console.log(JSON.stringify(error));
            });
        }         
 
    });
});